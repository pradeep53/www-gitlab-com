---
layout: handbook-page-toc
title: UnReview Overview
description: "UnReview is an approach for finding appropriate code reviewers in the most effective way"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introducing UnReview

### Overview

Code review is an important part of any development process. Over the years, code review has shown a large impact on improving the quality of source code. In addition, code review facilitates in transferring knowledge about the codebase, approaches, expectations regarding quality, etc.; both to the reviewers and to the author.

In a small project, finding an appropriate code reviewer isn't a big issue. This might be done manually. However, as the project grows, finding an appropriate person becomes increasingly difficult. In this setting, random assignments will be potentially error-prone and therefore don't appear to be efficient. The other option of always assigning key reviewers would make the selected persons overburdened and a bottleneck due to inadvertently siloing knowledge. To address the code reviewer recommendation problem, the UnReview project has been initiated.

UnReview recommends reviewers, considering their experience in the part of the source code proposed by a merge request. UnReview is able to resolve the cold start problem, i.e., when the proposed source code is unknown to the recommendation engine, and it additionally tries to balance the review load across the team. Future versions will also consider the context of the merge request, i.e., which source code has been exactly changed and how it affects other parts of the project.

Today, UnReview is an early-stage technology.  However, significant testing and validation has been done on production data. After the [acquisition](https://about.gitlab.com/press/releases/2021-06-02-gitlab-acquires-unreview-machine-learning-capabilities.html) is complete, we continue to work on the approach, integrating UnReview into GitLab via iteration.

### Architecture

UnReview consists of multiple components used for a variety of purposes, from data extraction and processing to training machine learning models:

* [Apache Kafka](https://kafka.apache.org/): distributed event streaming and processing;
* [Apache Hive](https://hive.apache.org/): data preprocessing including building train/test datasets;
* [Azure Data Factory](https://azure.microsoft.com/en-us/services/data-factory/): orchestrating the preprocessing pipelines, managing the Hive cluster, and moving the processed data to MongoDB
* [MongoDB](https://www.mongodb.com/): document-based database for storing the processed data
* [Azure Blob Storage](https://azure.microsoft.com/en-us/services/storage/blobs/): temporary storage for the trained ML models and raw data

The following chart provides more details on how the UnReview components relate to each other:

```mermaid
flowchart TD

subgraph Azure
    ExtractStage --> |Kafka|TransformStage
    TransformStage --> |Azure Data Factory|LoadStage
    TransformStage --> |manual start of training|TrainStage
    TrainStage --> LoadStage
    LoadStage --> UnreviewBackend 

    subgraph ExtractStage
        GitLab --> |API| Extracteur
        Git --> |API| Extracteur
    end

    subgraph TransformStage
        AzureBlobStorage[(AzureBlobStorage)]
        AzureBlobStorage --> |Hive, Azure Data Factory| AzureBlobStorage
    end

    subgraph LoadStage
        Mongo[(ProcessedData/MongoDB)]
        Models[(TrainedModels/AzureBlobStorage)]
    end

    subgraph TrainStage
        ModelTraining <--> HyperparameterTuning --> BestModelSelection
    end

    subgraph UnreviewBackend
        Artefacts
        Recommender
        Analytics
    end
end
```

When integrating UnReview into GitLab, some components can be replaced as we progress through the defined milestones.

### Integration into GitLab

The backend work for integration will be primarily handled by the [Applied ML](https://about.gitlab.com/handbook/engineering/development/modelops/appliedml/) team with help from the infrastructure team.  The frontend work will be by both the Applied ML team and the `Create::code review` team (PM [Kai Armstrong](https://about.gitlab.com/company/team/#phikai) and EM [André Luís](https://about.gitlab.com/company/team/#andr3)).

* Milestone 1 [Reviewer/maintainer assignment architectural plan and PoC](https://gitlab.com/groups/gitlab-org/-/epics/5794)
* Milestone 2 [Customer facing MVC of integration of unreview](https://gitlab.com/groups/gitlab-org/-/epics/6113)
* Milestone 3 [Extend MVC and create plan for unreview functionality for self-hosted customers](https://gitlab.com/groups/gitlab-org/-/epics/6114)

## Milestone 1

### Overview

Milestone 1 focuses on creating an UnReview proof-of-concept that works like [Reviewer Roulette](https://docs.gitlab.com/ee/development/code_review.html#reviewer-roulette) based on the GitLab product code. This milestone retains the existing UnReview functionality but requires a number of changes to the architectural components of the approach. 

The following tasks have to be completed:

* Move Azure and GitHub-hosted aspects of UnReview to GCP.
* Implement UnReview code and dependent components in the GitLab managed GCP instance.
* Replace the small number of libraries with undesirable open-source licenses.
* Demonstrate UnReview on the GitLab codebase and compare/contrast with existing Reviewer-Roulette. 

More information on Milestone 1 can be found by following its [epic](https://gitlab.com/groups/gitlab-org/-/epics/5794).

## Milestone 2

### Overview

Milestone 2 focuses on providing the UnReview functionality to GitLab.com customers and for dogfooding at GitLab. At this milestone, UnReview should be able to connect to a provided project, automatically extract/process data, and periodically retrain the ML models to improve code reviewer recommendations. GitLab.com customers will start seeing and experiencing value from the functionality, including the ability to intelligently assign code reviewers to merge requests based on ML models.

To support GitLab.com customers: 

* Code reviewer recommendations should be implemented behind the feature flag, disabled by default.
* Integrate the UnReview UI into the GitLab product following the MVC pattern.
* Make it possible to compare UnReview with existing methods by providing recommendations in a comment.
* Add the option to automatically assign reviewers recommended by UnReview.

To support dogfooding at GitLab: 

* Start the dogfooding process.
* Make UnReview available to replace or exist alongside Review Roulette for GitLab projects.
* Automatically assign reviewers recommended by UnReview.

More information on Milestone 2 can be found by following its [epic](https://gitlab.com/groups/gitlab-org/-/epics/6113).

## Milestone 3:

### Overview:

Milestone 3 focuses on further improving the UnReview functionality for Gitlab.com customers, as well as providing similar functionality to self-hosted customers. At this milestone, GitLab.com customers will continue seeing and experiencing value from the functionality, including the ability to intelligently assign code reviewers to merge requests based on ML models. Primary feedback from GitLab.com customers and dogfooding at GitLab should be considered and integrated into the product. 
	
To support GitLab.com customers:

* Make the feature flag enabled by default and available to customers GA via configuration options in the GitLab user interface.
* Automatically assign reviewers when the feature flag is enabled.

To support self-hosted customers:

* Determine self-hosted performance concerns as it relates to performance and storage requirements in the customer-hosted environment and at customers that intentionally have no internet connectivity.

More information on Milestone 3 can be found by following its [epic](https://gitlab.com/groups/gitlab-org/-/epics/6114).



