---
layout: job_family_page
title: "Developer Evangelists"
---

## Developer Evangelist

The Developer Evangelism team supports and grows GitLab's community by engaging with GitLab community members through content and conversations.

As a Developer Evangelist, you will help us fulfill our mission by connecting with other developers, contributing to open source, and sharing your knowledge and experience about GitLab and other leading technologies at conferences and meetups, in contributed articles, and on blogs, podcasts, and social media. Your work will foster a community inspired by GitLab and will drive our strategy around developer love and GitLab’s participation in the open source ecosystem.

We focus on generating awareness about GitLab by rolling up our sleeves, contributing to the ecosystem, and enabling others to become evangelists outside the company as well. Not afraid to be hands-on, you might write sample code, author client libraries, provide insights to journalists, and work with strategic GitLab partners such as GitLab Heroes, users, and customers to excite and engage our developer communities.

## Developer Evangelist

### Job Grade

The Developer Evangelist is a [grade 6](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* Lead the conversation around the latest technology advancements and best practices in the developer community
* Create engaging content, including technical talks, blog posts, demos, and videos, that educates developers on important technologies and trends
* Support GitLab's product and engineering efforts by sharing what you learn while engaging with the wider GitLab community and the tech community, at large
* Conduct interviews with media via phone, podcasts, video, and in-person
* Contribute to relevant open source projects, foundations, and SIGs in order to give GitLab visibility and share our experience and insights to developments in our areas of interest
* Be a leader within GitLab and in the wider community

### Requirements

* Experience building software and contributing to open source in the cloud computing ecosystem
* At least 1 year of experience giving talks and developing demos, workshops, webinars, videos, and other technical content
* Meaningful social presence with engaged followers
* Ability to manage the fast moving conference schedule with its CFP deadlines and show dates
* Self-directed and work with minimal supervision
* Outstanding written and verbal communications skills with the ability to translate complex technology concepts into simple and intuitive communications
* Ability to travel up to 30% of the time
* You share our [values](/handbook/values/) and work in accordance with those values
* Ability to use GitLab

## Senior Developer Evangelist

### Job Grade

The Senior Developer Evangelist is a [grade 7](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Requirements

* Same as Developer Evangelist plus,
* 2-3 years experience giving talks and developing demos, webinars, videos, and other technical content to audiences of 300 and larger
* Experience serving as a media spokesperson

## Staff Developer Evangelist

### Requirements

* Same as Senior Developer Evangelist plus,
* Hold positions of influence in open source projects and organizations such as SIG leads, maintainer status, author status
* A growing and engaged social following of 5k+ followers or equivalent
* Experience giving talks and developing demos, webinars, videos, and other technical content as keynote speaker
* Proven leader who has contributed to the success of other GitLab team members 

### Job Grade

The Staff Developer Evangelist is a [grade 8](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

## Manager, Developer Evangelism

As the Manager, Developer Evangelism, you will be responsible for building our developer evangelism function with the ultimate objectives of:

* Driving awareness of GitLab as the single application for the entire DevOps cycle
* Amplifying GitLab's Cloud Native thought leadership in DevSecOps
* Beginning conversations about GitLab as the leading cloud-agnostic player
* Inspiring and empowering our community to become developer evangelists
* Increasing GitLab excitement among developers and SREs

### Job Grade

The Manager, Developer Evangelism is a [grade 8](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* Define and execute the Developer Evangelism vision and strategy and collaborate to align it across teams
* Identify opportunities and build the resources to equip the GitLab team and the wider community to become developer evangelists
* Create, report, and iterate on the relevant key performance indicators to effectively measure the impact of GitLab's developer evangelism initiatives
* Mentor, guide, and grow the careers of all team members
* Build and continually evolve the team's processes to make them more effective
* Enable the Developer Evangelism team to produce and execute their quarterly OKRs
* Develop a hiring plan according to the dynamic needs of a rapidly growing organization

### Requirements

* You have 3-5 years of experience running a developer relations program, preferably open source in nature.
* You have 2+ years of experience leading a team of developer evangelists/advocates.
* Broad knowledge of the DevOps landscape and key players. Experience and connections in the Cloud Native ecosystem are a plus.
* Analytical and data driven in your approach to building and nurturing communities.
* You have experience facilitating sensitive and complex community situations with humility, empathy, judgment, tact, and humor.
* Excellent spoken and written English.
* Familiarity with developer tools, Git, Continuous Integration, Containers, and Kubernetes.
* Ability to use GitLab.
* You share our [values](https://about.gitlab.com/handbook/values/), and work in accordance with those values.

## Developer Evangelism Program Manager

**Mission:** Build a powerhouse developer evangelism program at GitLab that encompasses teammates and the wider community. Create a program to increase GitLab’s awareness in the wider community.

The Developer Evangelism Program Manager is a Developer Evangelist, who focuses on managing the processes and ensure we collect the right metrics. As part of this role you will have the opportunity to work closely with deeply technical leaders and marketing experts. You will be coached to develop your own technical thought leadership platform and also have the opportunity to grow your career as a technical marketer.

### Job Grade

The Developer Evangelism Program Manager is a [grade 6](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* Develop speaker’s bureau database that includes speakers and thought leadership platforms
* Manage the conferences' Call For Proposals (CFP) process, working hand-in-hand with internal and external thought leaders on their talk submissions
* Review slide decks and develop a speaker training program
* Develop slide deck formats for speaker’s that include slides of interest to GitLab campaigns
* Support developer evangelism content development in line with thought leadership platforms
* Create engaging content, including technical talks, blog posts, demos, and videos, that educates developers on important technologies and trends
* Liaison with PR and content to execute on evangelism campaigns, as well as extend the effort of recorded talks at various events
* Maintain the Developeer Evangelism Handbook page as a rich resource for anyone looking into Developer Evangelism.
* Work hand-in-hand with the community relations team to extend the evangelism program’s reach.
* Define metrics of measurement with the Manager, Developer Evangelism
* Instrument for metrics of measurement and report on them regularly
* Build thought leadership in GitLab product areas over time

### Requirements

* Excellent written and verbal communication skills
* Background in or deep curiosity about the cloud computing and devops ecosystem
* Track record of success in a software company
* At least 3-5 years work experience in a fast-paced working environment
* Exceptional organizational skills
* Technical skills, particularly an understanding of DevOps and cloud native technologies
* Relationships in the software DevSecOps space are a plus

## Senior Developer Evangelism Program Manager

### Requirements 
* Same as Developer Evangelism Program Manager
* Continuously improve metrics collection, goal setting, and reporting to help team increase contribution to GitLab company goals
* Uplevel the speaker's bureau to an influencer bureau and run the program with all modes of content creation in mind - speaking, technical demos, blog posts, media, etc.
* Meaningful impact to overall team impressions KPI from blog posts, technical talks and demos
* DRI integrated campaigns with other GitLab teams to achieve the above

## Career Ladder

The next step for both individual contributors and managers of people is to move to the [Director, Technical Evangelism](/job-families/marketing/director-technical-evangelism) job family.

## Performance indicators

* Impressions per month. Number of combined impressions/organic views from the TE team, from a designated number of sources (e.g. social, events, content, etc.)

## Specialties 

### Community Engagement 

The Developer Evangelism team is responsible for [community engagement](/handbook/marketing/community-relations/developer-evangelism/#community-engagement) with the GitLab community and the tech community at large as it relates to GitLab. A Developer Evangelist with a Community Engagement specialty owns that engagement. This includes responding to questions about GitLab on Hacker News, engaging with the GitLab community on Stack Overflow, managing the GitLab Forum, and providing insight and guidance to GitLab's marketing and product teams on the community impact of changes when we are making to our product and business. 

#### Responsibilities 

* Provide thoughtful responses to community member questions on online forums monitored by GitLab's Developer Evangelist team including [Hacker News](/handbook/marketing/community-relations/developer-evangelism/hacker-news/), StackOverflow, [GitLab's blog](/handbook/marketing/community-relations/developer-evangelism/hacker-news/#blog-comments) and the [GitLab Forum](https://forum.gitlab.com/). This involves cross-team collaboration including encouraging other team members to engage with the community and providing feedback to product and engineering teams. 
* Lead the Developer Evangelist team's support of [community response](/handbook/marketing/community-relations/developer-evangelism/community-response/) situations. This includes:
  * Manage responsibilities and tasks associated with product and/or business changes that impact the community
  * Ensure messaging about these changes addresses community concerns
  * Create community FAQs for impactful changes
  * Prepare the Developer Evangelist team and other relevant team members per the [community response](/handbook/marketing/community-relations/developer-evangelism/community-response/) process to address community questions on Hackers News and other forums


#### Requirements 

* Same as Developer Evangelist plus,
* Be a natural communicator and enjoy using those skills to help others
* Comfortable communicating with people via public forums, in line with GitLab's transparency value 
* Ability to inspire team members across GitLab to engage with directly with members of the wider GitLab community
* Experience managing or participating in online communities. Experience engaging with online technical communities is a plus.
